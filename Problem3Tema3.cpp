#include <iostream>
#include <math.h>
#include <string>

using namespace std;

int main()
{
cout << "Enter a number in meters:" << endl;
double meters;
cin >> meters;

cout << meters << " m = " << meters / 1000 << " km" << endl;
cout << meters << " m = " << meters * 10 << " dm" << endl;
cout << meters << " m = " << meters * 1000 << " cm" << endl;
}
